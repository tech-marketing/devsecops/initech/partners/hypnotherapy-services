FROM golang:1.17-alpine

RUN apk update

COPY ./go.mod /app/go.mod

WORKDIR /app
RUN go mod download
COPY . /app

RUN go build -o /hypno

EXPOSE 8091
CMD [ "/hypno" ]