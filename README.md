# Hypnotherapy

This is just a little microservice that tells you how much
sleep you need depending on the request you send.

## Usage

1. Run the application
```
$ go run main.go
```
2. Open a separate terminal

3. Send a request to the application
```
$ curl -X POST -d '{"message":"yoooo"}' 0.0.0.0:8091/sleep

5
```
